db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$name", avePrice: {$avg: "$stock"}}}
]);

db.fruits.aggregate([
    {$group: {_id: "$supplier_id", avePrice: {$avg: "$stock"}}}
]);

